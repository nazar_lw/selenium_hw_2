import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static java.util.concurrent.TimeUnit.SECONDS;
import static utils.Constants.*;

public class TestGmail {

    @Test
    public void testLoginAndSend() {

        System.setProperty(CHROME_DRIVER, CHROME_DRIVER_PATH);

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, SECONDS);
        driver.get(MAIN_URL);

        WebElement emailInput = driver.findElement(By.id("identifierId"));
        emailInput.sendKeys(EMAIL);
        WebElement proceedWithEmailButton = driver.findElement(By.cssSelector("div.VfPpkd-RLmnJb"));
        proceedWithEmailButton.click();

        WebElement passwordInput = driver.findElement(By.cssSelector(".I0VJ4d > div:nth-child(1) > input"));
        passwordInput.sendKeys(PASSWORD);
        WebElement proceedWithPasswordButton = driver.findElement(By.cssSelector("div.qhFLie:nth-child(1)"));
        proceedWithPasswordButton.click();
        
        WebElement composeButton = driver.findElement(By.cssSelector("div.T-I.T-I-KE.L3"));
        composeButton.click();

        WebElement recipientInput = driver.findElement(By.xpath("//textarea[@class='vO']"));
        recipientInput.clear();
        recipientInput.sendKeys(RECIPIENT_EMAIL);

        WebElement subjectInput = driver.findElement(By.name("subjectbox"));
        subjectInput.clear();
        subjectInput.sendKeys(SUBJECT);

        WebElement mainTextArea = driver.findElement(By.xpath("//div[@class='Am Al editable LW-avf tS-tW']"));
        mainTextArea.clear();
        mainTextArea.sendKeys(CONTENT);

        WebElement sendEmailButton = driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']"));
        sendEmailButton.click();

        WebElement getAllSentEmailsButton = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(40))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.TN.bzz.aHS-bnu")));
        getAllSentEmailsButton.click();
        getAllSentEmailsButton.click();
        
        WebElement lastLetter =
                driver.findElement(By.xpath("//div[@class='BltHke nH oy8Mbf']//div[@class='Cp' and preceding-sibling::div[@class='Cp']]//table/tbody/tr[1]"));
        lastLetter.click();

        WebElement letterView = driver.findElement(By.xpath("//td[@class='Bu bAn']"));
        File scrFile = letterView.getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File("screenshot\\letterView.png"));
            System.out.println("Screenshot captured successfully!!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        WebElement subjectOfTheLetterTag = driver.findElement(By.className("hP"));
        Assert.assertEquals(subjectOfTheLetterTag.getText(), SUBJECT, " Subject of the letter does not match ! ");

        WebElement accountWindow = driver.findElement(By.xpath("//img[@class='gb_La gbii']"));
        accountWindow.click();
        WebElement exitButton = driver.findElement(By.id("gb_71"));
        exitButton.click();

        driver.quit();
    }
}


//  /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div/table/tbody/tr[1]